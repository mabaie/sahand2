const path = require('path');
const webpack = require('webpack');
//const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
var CompressionPlugin = require("compression-webpack-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
module.exports = {
    entry: './sahand-web/src/index.js',
    mode: 'development',
    module: {
        rules: [{
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                options: {
                    presets: ['env', 'react', 'stage-2']
                },
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader', 'eslint-loader']
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/, 
                use: [{
                    loader: 'file-loader', 
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'images'
                    }
                }]
            },
        ]
    },
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    output: {
        path: path.resolve('/home/edris/Desktop/sahand/sahand2/sahand-web/dist/'),
        publicPath: '/sahand/dist/',
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: path.join(__dirname, 'sahand-web/public/'),
        port: 3000,
        publicPath: 'http://localhost:3000/dist/',
        hotOnly: true
    },
    devtool: 'cheap-module-source-map',
    stats: {
        colors: true,
        hash: true,
        timings: true,
        assets: true,
        chunks: true,
        chunkModules: true,
        modules: true,
        children: true,
    },
    optimization: {
        minimizer: [
            new UglifyJSPlugin({
                sourceMap: true,
                uglifyOptions: {
                    compress: {
                        inline: false
                    }
                }
            })
        ],
        runtimeChunk: false,
        splitChunks: {
            chunks: 'all',
            minSize: 30000,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            automaticNameDelimiter: '~',
            name: true,
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true
                }
            }
        }
    },
    plugins: [
        //new webpack.HotModuleReplacementPlugin(),
        // new BundleAnalyzerPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"development"'
        }),
        new webpack.IgnorePlugin(/^\.\/locale$/, [/moment$/]),
        new CompressionPlugin({
            filename: "[path].gz[query]",
            algorithm: "gzip",
            threshold: 10240,
            minRatio: 0
        })
    ],
    watch: true,
    externals: {
        'fullcalendar': 'fullcalendar',
        'Quill': 'Quill',
        'jquery': 'jQuery',
        'axios': 'axios',
        'ReactRouter': 'react-router',
        'ReactRouterDom': 'react-router-dom',
        'core-js': 'core-js-bundle',
        'joi-browser': 'Joi',
        'lodash': '_',
        'moment': 'moment',
        'react': 'React',
        'react-dom': 'ReactDOM',
        'react-addons-transition-group': 'var React.addons.TransitionGroup',
        'react-addons-pure-render-mixin': 'var React.addons.PureRenderMixin',
        'react-addons-create-fragment': 'var React.addons.createFragment',
        'react-addons-update': 'var React.addons.update',
        '@material-ui/core': "window['material-ui']",
        'jss': 'jss',
        'tui-calendar': "tui['Calendar']",
    },
    watchOptions: {
        ignored: ['node_modules']
    },
};