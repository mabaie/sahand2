'use strict'
const ObjectID = require('mongodb').ObjectID;
const ProfileModel = require('../../../Models/profile').ProfileModel;
module.exports = async function(req, res, next){
    try{
        const t = await ProfileModel.findOne({_id: new ObjectID(req.params.id)});
        res.json({image:t[0].image});
    }catch(err){
        res.json({});
    }
}