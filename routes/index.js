'use strict';

const express = require('express');
const router = require('express').Router();
const apiRouter = require('./api');

router.use('/sahand/api', apiRouter);
router.use('/sahand/dist', express.static('/home/edris/Desktop/sahand/sahand2/sahand-web/dist'))
router.use('/sahand/*', express.static('/home/edris/Desktop/sahand/sahand2/sahand-web/public'));
// router.use('/sahand/dist', express.static('/var/www/html/dist'))
// router.use('/sahand/*', express.static('/var/www/html/public'));

module.exports = router;
